FROM python:3.7.10-alpine
ENV PYTHONUNBUFFERED=1

WORKDIR /usr/src/app

# install psycopg2 et graphviz dependencies
RUN apk add build-base postgresql-dev
# install the app dependencies
COPY ./requirements.txt /usr/src/app
RUN pip install -r requirements.txt

# install uwsgi (env similaire a une prod)
RUN apk add linux-headers pcre-dev
RUN pip install uwsgi

EXPOSE 8000
