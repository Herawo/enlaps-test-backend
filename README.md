# Enlaps-Test-Backend

Mini projet de test pour développeur backend chez Enlaps

## Install

`cp .env.dist .env`  
`docker compose build`  
`docker ps` -> récupérer l'id du conteneur  
`docker exec -it [container_id] sh`  
`./manage.py createsuperuser`  
`docker compose up`  

## Lancement des scripts
`docker exec -it [container_id] python manage.py digest photo_list.json`  
`docker exec -it [container_id] python manage.py stitch`  

