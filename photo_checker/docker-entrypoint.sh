python manage.py collectstatic --noinput
python manage.py migrate

if [ $APP_ENV = "production" ]; then
    uwsgi --ini uwsgi.ini --http-socket :8000
else
    python manage.py runserver 0.0.0.0:8000
fi