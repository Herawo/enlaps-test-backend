from django.contrib import admin

from photo.models import PhotoStorage

admin.site.register(PhotoStorage)