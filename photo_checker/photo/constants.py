PHOTO_MATCHING_FIELDS = {
    "tikee_uuid": {
        "json_field": ['s3_key', ],
        "method": 'get_tikee_uuid',
    },
    "sequence_id": {
        "json_field": ['s3_key', ],
        "method": 'get_sequence_id',
    },
    "photo_type": {
        "json_field": ['s3_key', ],
        "method": 'get_photo_type',
    },
    "shooting_date": {
        "json_field": ['shooting_date', ],
        "method": '',
    },
    "file_size": {
        "json_field": ['file_size', ],
        "method": '',
    },
    "link": {
        "json_field": ['s3_key', ],
        "method": '',
    },
    "resolution": {
        "json_field": ['resolution', ],
        "method": '',
    },
    "gps_latitude": {
        "json_field": ['metadata', 'GPSLatitude'],
        "method": '',
    },
    "gps_longitude": {
        "json_field": ['metadata', 'GPSLongitude'],
        "method": '',
    },
    "gps_altitude": {
        "json_field": ['metadata', 'GPSAltitude'],
        "method": '',
    },
    "camera_model": {
        "json_field": ['metadata', 'Camera Model Name'],
        "method": '',
    },
    "make": {
        "json_field": ['metadata', 'Make'],
        "method": '',
    },
}