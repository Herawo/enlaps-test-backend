# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import gettext as _

# App imports
from photo.parsers import PhotoParser
from photo.models import PhotoStorage


class Command(BaseCommand):
    help = _("Digests a JSON to create a PhotoStorage instance")

    def add_arguments(self, parser):
        parser.add_argument('filename', type=str)

    def handle(self, *args, **options):
        filename = options['filename']
        parser = PhotoParser()
        json_data = parser.get_object_to_parse(**{'filename': filename})
        parser.parse(json_data)