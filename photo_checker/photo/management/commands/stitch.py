# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import gettext as _

# App imports
from photo.models import PhotoStorage
from photo.exceptions import StitchingException

class Command(BaseCommand):
    help = _("Stitch")

    def handle(self, *args, **options):
        for photo in PhotoStorage.objects.all():
            try:
                photo.stitch()
            except StitchingException as e:
                print('%s' % e)
                