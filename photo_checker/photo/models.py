# Django imports
from django.db import models
from django.utils.translation import gettext as _

# App imports
from photo.exceptions import StitchingException


# Un seul modele. On aurait pu avoir une répartition par séquence avec une 
# modele séquence lié a plusieurs tirages mais ça fait une jointure inutile
# alors qu'on peut gérer facilement les séquences grace aux sequence_id

class PhotoStorage(models.Model):

    PHOTO_TYPE_LEFT = 'left'
    PHOTO_TYPE_RIGHT = 'right'
    PHOTO_TYPE_STITCHED = 'stitched'
    PHOTO_TYPE_CHOICES = (
        (PHOTO_TYPE_LEFT, _("Left")),
        (PHOTO_TYPE_RIGHT, _("Right")),
        (PHOTO_TYPE_STITCHED, _("Stitched")),
    )
    tikee_uuid = models.CharField(
        max_length=200,
        verbose_name=_("Identifiant du Tikee")
    )
    sequence_id = models.CharField(
        max_length=200,
        verbose_name=_("Identifiant séquence")
    )
    photo_type = models.CharField(
        max_length=200,
        choices=PHOTO_TYPE_CHOICES,
        verbose_name=_("Type de photo")
    )
    shooting_date = models.DateTimeField(
        verbose_name=_("Date de la capture")
    )
    file_size = models.IntegerField(
        verbose_name=_("Taille du fichier")
    )
    link = models.CharField(
        max_length=200,
        verbose_name=_("Lien vers la photo")
    )
    resolution = models.CharField(
        max_length=200,
        verbose_name=_("Résolution de la photo")
    )
    gps_latitude = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=True, blank=True,
    )
    gps_longitude = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=True, blank=True,
    )
    gps_altitude = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=True, blank=True,
    )
    camera_model = models.CharField(
        max_length=200,
        verbose_name=_("Modele de camera"),
        null=True, blank=True,
    )
    make = models.CharField(
        max_length=200,
        verbose_name=_("Auteur"),
        null=True, blank=True,
    )

    class Meta:
        unique_together = ('tikee_uuid', 'sequence_id', 'photo_type')

    def __str__(self):
        return "{} {}... {}".format(self.tikee_uuid, self.sequence_id[0:6], self.photo_type)

    def stitch(self):
        photos = self.__class__.objects.filter(
            tikee_uuid=self.tikee_uuid, sequence_id=self.sequence_id
        )
        other_type = self.PHOTO_TYPE_LEFT \
                     if self.photo_type == self.PHOTO_TYPE_RIGHT \
                     else self.PHOTO_TYPE_RIGHT
        if not photos.filter(photo_type=other_type).exists():
            raise StitchingException(_(
                "Missing photo type {} for {} {}.".format(
                    other_type, self.tikee_uuid, self.sequence_id
                )
            ))
        if photos.filter(photo_type=self.PHOTO_TYPE_STITCHED).exists():
            raise StitchingException(_(
                "Already existing photo type {} for {} {}.".format(
                    self.PHOTO_TYPE_STITCHED, self.tikee_uuid, self.sequence_id
                )
            ))
        print(
            "{} {} {}  has been sent to the stitching service".format(
                self.photo_type, self.tikee_uuid, self.sequence_id
            )
        )
