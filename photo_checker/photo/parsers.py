# Standard imports
import json
import os
from copy import deepcopy

# App imports
from photo.constants import PHOTO_MATCHING_FIELDS
from photo.exceptions import ParsingException
from photo.models import PhotoStorage

# Django imports
from django.utils.translation import gettext as _
from django.conf import settings
from django.db.utils import IntegrityError


class BaseParser(object):

    model = None
    matching_dict = None

    def parse(self, to_parse):
        """
        This methods should parses an object to fill a `model` instance 
        according to the `matching_dict` attribute. 
        Make your own implementation based on the type of input you have.
        """
        raise Exception(_("Not implemented"))

    def get_object_to_parse(self, *args, **kwargs):
        """
        This methods should return the object needed by `parse` method.
        Could be local storage, HTTP request, manual input, ...
        Make your own implementation based on the type of input you have.
        """
        raise Exception(_("Not implemented"))


class JsonParser(BaseParser):

    def get_real_value(self, data, field_list, iter_nb=100):
        """
        Parses the list of fields, getting the value each timeand popping 
        the item from the list. Accessing to values ​​in the json dynamically 
        using this method involves parsing a copy of the dictionary and not
        the dictionary itself otherwise the pop () would erase all the fields
        and it will end up with None everywhere
        """
        if not field_list or not data:
            return None
        iter_nb -= 1
        if iter_nb < 0:
            raise Exception(_("Max iteration reached"))
        key = field_list.pop(0)
        value = data.get(key, {})
        if len(field_list) == 0:
            return value
        else:
            return self.get_real_value(value, field_list, iter_nb)


class PhotoParser(JsonParser):

    Model = PhotoStorage
    matching_dict = PHOTO_MATCHING_FIELDS

    def get_object_to_parse(self, *args, **kwargs):
        filename = kwargs.get('filename')
        path = os.path.join(settings.STATIC_ROOT, filename)
        file = open(path, 'r')
        data = json.load(file)
        file.close()
        return data

    def parse(self, to_parse):
        for photo in deepcopy(to_parse):
            new_instance = self.Model()
            for field, desc in self.matching_dict.items():
                value = self.get_real_value(photo, deepcopy(desc.get('json_field')))
                if desc.get('method'):
                    value = getattr(self, desc.get('method'))(value)
                setattr(new_instance, field, value)
            try:
                self.check_resolution(
                    tikee_uuid=new_instance.tikee_uuid,
                    sequence_id=new_instance.sequence_id,
                    value=new_instance.resolution,
                )
            except ParsingException as e:
                print('%s' % e)
            try:
                new_instance.save()
                print("imported photo {} {}".format(
                    new_instance.tikee_uuid, new_instance.sequence_id)
                )
            except IntegrityError as e:
                print("%s" % e)
            

    def check_resolution(self, tikee_uuid, sequence_id, value):
        other_photos = self.Model.objects.filter(
            tikee_uuid=tikee_uuid, sequence_id=sequence_id
        )
        if other_photos.exists():
            for photo in other_photos:
                if photo.resolution != value:
                    raise ParsingException(_(
                        "Resolution conflict for photo {} {}. {} /// {}".format(
                            tikee_uuid, sequence_id, value, photo.resolution
                        )
                    ))

    def get_sequence_id(self, string):
        return string.split('/')[0]

    def get_tikee_uuid(self, string):
        return string.split('/')[1]

    def get_photo_type(self, string):
        return string.split('/')[2]
            
