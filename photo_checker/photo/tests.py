# Django imports
from django.test import TestCase

# App import
from photo.parsers import PhotoParser
from photo.exceptions import ParsingException

TEST_GET_REAL_VALUE = {
    'test_1_level': 'value_1',
    'test_2_level': {
        'test_2_level_first': 'value_2',
        'test_2_level_second': ['value_3', ],
    },
    'test_3_level': {
        'test_3_level_first': {
            'test_3_level_first_1': 'value_4',
            'test_3_level_first_2': ['value_5', ],
        },
        'test_3_level_second': 'value_6',
    },
    'exception': {
        '1': {'2':{'3':{'4': 'erreur'}}}
    }
}

class ParserTestCase(TestCase):
    def setUp(self):
        self.parser = PhotoParser()

    def test_get_real_value(self):
        level_1 = ['test_1_level',]
        level_2_1 = ['test_2_level', 'test_2_level_first', ]
        level_2_2 = ['test_2_level', 'test_2_level_second', ]
        level_3_1_1 = ['test_3_level', 'test_3_level_first', 'test_3_level_first_1', ]
        level_3_1_2 = ['test_3_level', 'test_3_level_first', 'test_3_level_first_2', ]
        level_3_2 = ['test_3_level', 'test_3_level_second', ]
        exception = ['exception', '1', '2', '3', '4', ]
        self.assertEqual(
            self.parser.get_real_value(TEST_GET_REAL_VALUE, level_1),
            'value_1'
        )
        self.assertEqual(
            self.parser.get_real_value(TEST_GET_REAL_VALUE, level_2_1),
            'value_2'
        )
        self.assertEqual(
            self.parser.get_real_value(TEST_GET_REAL_VALUE, level_2_2),
            ['value_3', ]
        )
        self.assertEqual(
            self.parser.get_real_value(TEST_GET_REAL_VALUE, level_3_1_1),
            'value_4'
        )
        self.assertEqual(
            self.parser.get_real_value(TEST_GET_REAL_VALUE, level_3_1_2),
            ['value_5', ]
        )
        self.assertEqual(
            self.parser.get_real_value(TEST_GET_REAL_VALUE, level_3_2),
            'value_6'
        )
        self.assertEqual(
            self.parser.get_real_value(TEST_GET_REAL_VALUE, exception),
            'erreur'
        )
        self.assertRaises(
            ParsingException, self.parser.get_real_value(
                data=TEST_GET_REAL_VALUE, field_list=exception, iter_nb=2
            ),
        )